import { Doughnut } from 'react-chartjs-2';  //named export

//letrs create a function that will describe the structure of our chart that will serve to visualize the data coming from our api service.
//i will identify the props that i want o inject/pass inside our chart component
//we want to pass multiple props inside our diagram
//what are the props that you want to display/show/visualize inside the component (1. data numbers(count). 2. label)
//we want to be able to changle/add multiple attributes to change/modify the figure of our chart. when passing multiple data sets yo have to describe its properties as an object. 
export default function DoughnutChart({criticals, deaths, recoveries}) {
	return(
		<Doughnut data={{
            datasets: [{
            	data: [criticals, deaths, recoveries], 
            	backgroundColor: ["red", "black", "green"]
            }],
            labels: ['Criticals', 'Mga Namatay', "Mga Gumaling"]
		}} redraw={false} />
		)
}