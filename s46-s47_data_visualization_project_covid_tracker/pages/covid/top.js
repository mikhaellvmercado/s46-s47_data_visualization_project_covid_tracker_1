import { Doughnut } from 'react-chartjs-2';  
import toNum from '../../helpers/toNum';
//this page will display the countries with the most number of covid cases

//the difference of the approach on how to fetch data and display using a single module. 

//lets now build the structure of this page. 
export default function Top({data}){

   console.log(data) //this is added to check if we are successful. 

   //lets get the list of countries in our collection
   const countryStatus = data.countries_stat
   //we only want to get the names of each country. 
   const country = countryStatus.map(countryStat => {
   	  return {
   	  	name: countryStat.country_name,
   	  	//we have to convert the data from the cases property into numeric
   	  	cases: toNum(countryStat.cases)
   	  }
   })

   //since we are trying to evaluate the top 10 countries according to their number of cases lets create to logic to come up with the result. 
   //the goal here is to sort in descending order the total number of cases. 

   //syntax of sort() arr.sort([compareFunction])
   //the a and b inside the argument section describes the firstEl and secondEl
   country.sort((a, b) => {
     //lets create a control structure that will compare the cases per country according to the sequence they were fetched from the provider. 
     //the comparison between the 2 element can lead to 3 possible scenarios.
     //1. true (a < b)
     //2. false (a > b)
     //3. equal (a = b)
     if(a.cases < b.cases) {
         return 1 //who decided the return?
     } else if(a.cases > b.cases) {
         return -1
     } 
     else {
        return 0
     }
   })
   //BUT WE STILL HAVE TO modify the logic above...why? because when we fetched the data from our provider the data is actually already in ascending order. 
   return(
    <>
      <h1>Top 10 Countries with the Highest Number of Cases</h1>
      <Doughnut data={{
       datasets: [{
          data: [country[0].cases, country[1].cases, country[2].cases, country[3].cases, country[4].cases], 
          backgroundColor: ["#133337", "#bada55", '#696969','#7fe5f0',' #ff0000'] //would this allow colors in hexadecimal values?
       }], //how are we going to call the objects inside our array
        //visualize [{}, {}, {}]
        labels: [country[0].name, country[1].name, country[2].name, country[3].name, country[4].name,] 
      }}/> 
    </>
   	)
}

//Practice Task:
//continue getting the top 10 countries and display it in the figure
/*
#133337 -> change USA
3- #696969
4 - #7fe5f0
5- #ff0000
5 mins . and screen shot and send in HO thread. 
*/
//lets gather the data from our provider. 
export async function getStaticProps() {
	//fetch the data from the endpoint and we have to make sure to indicate the correct details that describes the request. 
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
           "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
           "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})
	//lets make the response or the output readable/parsable for the browser.
	const data = await res.json()

	//return the information and pass them as props. 
	return {
		props: {
			data
		}
	}
}