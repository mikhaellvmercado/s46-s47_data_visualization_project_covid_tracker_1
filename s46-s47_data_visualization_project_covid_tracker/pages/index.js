import Head from 'next/head'
//the next thing is to import a jumbotron from react-bootstrap
import Jumbotron from 'react-bootstrap/Jumbotron' 
import toNum from '../helpers/toNum' //defaul export method


//we are going to be creating a COVID-19 tracking app. 
//lets pass on the object property as props for us to be able to inject the data we fetched from rapid API inside our component.
export default function Home({globalTotal}) {
  return (
    <div>
      <Head>
         <title>Covid-19 Tracker App</title>
         <link rel="icon" href="/favicon.ico" />
      </Head>
      <Jumbotron>
         <h1> Total Covid-19 Cases in the World: 
             <strong> {globalTotal.cases} </strong> 
         </h1>
      </Jumbotron>
    </div>
  )
}

export async function getStaticProps() {
	//fetch the data from our API endpoint
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})

	//the data that will be taken from the API readable has to readable in the browser.
	const data = await res.json() 
	//since the data is already converted into a json format making it readble/parsable to get its properties
	const countriesStats = data.countries_stat //this property is provided by the API services. this property will allow us to get data which describes the total of COVID cases in all countries. 

	//we are going to create a logic that will allow us to get the covid cases around the world. 
	//lets declare a variable that will hold the total number of cases. we need to indicate where the count will start 
	let total = 0
	//lets call on the variable that holds the number of cases of each country and we are going to take the count of covid cases per country. 
	countriesStats.forEach(country => {
		total += toNum(country.cases) //now the concern is the data/information gathered from rapid api is by default in a string data type, so applying a mathematical operation of addition would not be possible. 
	})

	//lets declare an object
	const globalTotal = {
		cases: total 
	}

	//lets return the object as props 
	return {
		props: {
			globalTotal
		}
	}
}
