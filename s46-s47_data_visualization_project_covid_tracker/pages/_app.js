import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from '../components/NavBar'; 
//i just want to put the display components inside a container
import { Container } from 'react-bootstrap' //named export

export default function MyApp({ Component, pageProps }) {
  return(
  	<div>  
  	    <NavBar /> 
  	    <Container>
  	        <Component {...pageProps} />  
  	    </Container>
       
  	</div>
  ) 
}


