//function is to convert string API result to number data type.
export default function toNum(str) {
  //convert string to array to gain access to our array methods
  const arr = [...str] //use a spread operator to acquire all string data captured from our API services.
  //lets filter out the commas inside the string.
  const filteredArray = arr.filter(element => element !== ",");
  //lets reduce the filtered data back into a single string without any commas.
  return parseInt(filteredArray.reduce((x, y) => x + y))	
} 


//visualized line number 4
//['3455666', '966355'] => ['3455666' + '966355']
// [3455666966355]

//for example makukuha niya ung data sa services na naka alphabetical order

//America = '300'
//Bangladesh = '600'
//900
//Canada = '1000'